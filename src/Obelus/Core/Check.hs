module Obelus.Core.Check where

import Control.Applicative
import Control.Monad
import Data.Map (Map)
import qualified Data.Map as Map

import Obelus.Core.Label (Label)
import qualified Obelus.Core.Label as Label
import Obelus.Core.Type (Type)
import qualified Obelus.Core.Type as Type
import Obelus.Core.Syntax (Syntax)
import qualified Obelus.Core.Syntax as Syntax

type Environment = Map String Type

typeCheck :: Environment -> Syntax -> Maybe Type
typeCheck env s =
    case s of
        Syntax.Integer _ -> Just Type.integer
        Syntax.Tuple ss -> Type.tuple <$> mapM (typeCheck env) ss
        Syntax.Record lss ->
          Type.Record <$> mapM (mapM (typeCheck env)) lss
        Syntax.Tag tag args t -> do
          argsT <- mapM (typeCheck env) args
          case t of
            Type.Sum alts -> do
              aTs <- lookup tag alts
              if argsT == aTs
                then return t
                else Nothing
            _ -> Nothing
        Syntax.Variable x ->
          Map.lookup x env
        Syntax.Lambda p t b -> do
          env' <- match p t
          bt <- typeCheck (Map.union env' env) b
          Just (Type.function t bt)
        Syntax.Apply l r -> do
          lt <- typeCheck env l
          rt <- typeCheck env r
          Type.functionMatch lt rt
        Syntax.Project s l -> do
          t <- typeCheck env s
          case t of
            Type.Record fields -> Type.recordMatch l fields
            _ -> Nothing
        Syntax.Let p e b -> do
          xt <- typeCheck env e
          env' <- match p xt
          typeCheck (Map.union env' env) b
        Syntax.Case e alts -> do
          t <- typeCheck env e
          rts <- mapM (caseMatch env t) alts
          foldl allSame (Just (head rts)) (tail rts)
               
match :: Syntax.Pattern -> Type -> Maybe Environment
match Syntax.WildcardPattern _ = Just Map.empty
match (Syntax.VariablePattern x) t = Just (Map.singleton x t)
match (Syntax.SumPattern tag args) (Type.Sum alts) = do
  altArgs <- lookup tag alts
  if length args == length altArgs
    then do 
      ctxs <- zipWithM match args altArgs
      foldM (\a b -> Just (Map.union a b)) Map.empty ctxs
    else Nothing
match (Syntax.RecordPattern pats) (Type.Record fields) = 
  foldM match' Map.empty pats
  where 
    match' ctx (l, p) = do
      v <- lookup l fields
      ctx' <- match p v
      return (Map.union ctx' ctx)

caseMatch env t (p, s) = do
  env' <- match p t
  typeCheck (Map.union env' env) s

allSame Nothing _ = Nothing
allSame (Just t1) t2 =
  if t1 == t2
    then Just t1
    else Nothing