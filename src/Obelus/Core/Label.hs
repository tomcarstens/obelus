module Obelus.Core.Label where

data Label
  = Index Int
  | Label String
  deriving (Show, Eq)
