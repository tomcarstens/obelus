module Obelus.Core.Syntax where

import Obelus.Core.Label (Label)

import Obelus.Core.Type (Type)
import qualified Obelus.Core.Type as Type

data Syntax
    = Integer Integer
    | Tuple [Syntax]
    | Record [(Label, Syntax)]
    | Tag String [Syntax] Type
    | Variable String
    | Lambda Pattern Type Syntax
    | Apply Syntax Syntax
    | Project Syntax Label
    | Let Pattern Syntax Syntax
    | Case Syntax [(Pattern, Syntax)]
    deriving (Show, Eq)

data Pattern
  = WildcardPattern
  | VariablePattern String
  | RecordPattern [(Label, Pattern)]
  | SumPattern String [Pattern]
  deriving (Show, Eq)
