module Obelus.Core.Evaluate where

import Control.Monad
import Data.Map (Map)
import qualified Data.Map as Map

import Obelus.Core.Label (Label)
import qualified Obelus.Core.Label as Label
import Obelus.Core.Type (Type)
import qualified Obelus.Core.Type as Type
import Obelus.Core.Syntax (Syntax)
import qualified Obelus.Core.Syntax as Syntax

data Value
    = Integer Integer
    | Tag String [Value]
    | Record [(Label, Value)]
    | Closure Syntax.Pattern Syntax Context
    deriving (Show, Eq)

type Context = Map String Value

evaluate :: Context -> Syntax -> Maybe Value
evaluate ctx s =
    case s of
        Syntax.Integer i -> Just (Integer i)
        Syntax.Tuple fields -> do
          values <- mapM (evaluate ctx) fields
          Just (Record (zip (map Label.Index [0..]) values))
        Syntax.Record fields -> do
          fields' <- mapM (mapM (evaluate ctx)) fields
          Just (Record fields')
        Syntax.Tag t args _ -> do
          args' <- mapM (evaluate ctx) args
          Just (Tag t args')
        Syntax.Variable x ->
            Map.lookup x ctx
        Syntax.Lambda p _ b -> Just (Closure p b ctx)
        Syntax.Apply l r -> do
            lv <- evaluate ctx l
            rv <- evaluate ctx r
            case lv of
                Closure p b ctx' -> do
                  ctx'' <- match p rv 
                  evaluate (Map.union ctx'' ctx') b
                Integer _ -> error "Error! Integers are not functions"
                Record _ -> error "Error! Records are not functions"
        Syntax.Project s l -> do
          v <- evaluate ctx s
          case v of
            Record fields -> lookup l fields
            Integer _ -> error "Error! Projection on an Integer not supported"
            Closure{} -> error "Error! Projection on a closure not supported"
        Syntax.Let p e b -> do
            v <- evaluate ctx e
            ctx' <- match p v
            evaluate (Map.union ctx' ctx) b
        Syntax.Case e alts -> do
          v <- evaluate ctx e
          (ctx', b) <- firstMatch v alts
          evaluate (Map.union ctx' ctx) b

            
match :: Syntax.Pattern -> Value -> Maybe Context
match Syntax.WildcardPattern _ = Just Map.empty
match (Syntax.VariablePattern x) v = Just (Map.singleton x v)
match (Syntax.SumPattern t1 pats) (Tag t2 args) =
  if t1 == t2
    then do 
      ctxs <- zipWithM match pats args
      foldM (\a b -> Just (Map.union a b)) Map.empty ctxs
    else Nothing
match (Syntax.RecordPattern pats) (Record fields) = 
  foldM match' Map.empty pats
  where 
    match' ctx (l, p) = do
      v <- lookup l fields
      ctx' <- match p v
      return (Map.union ctx' ctx)

firstMatch :: Value -> [(Syntax.Pattern, Syntax)] -> Maybe (Context, Syntax)
firstMatch _ [] = Nothing
firstMatch x ((p, s):r) =
  case match p x of
    Just ctx' -> Just (ctx', s)
    Nothing -> firstMatch x r