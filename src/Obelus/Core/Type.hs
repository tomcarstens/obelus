module Obelus.Core.Type where

import Obelus.Core.Label (Label)
import qualified Obelus.Core.Label as Label

data Type
    = Constructor String
    | Record [(Label, Type)]
    | Sum [(String, [Type])]
    | Apply Type Type
    deriving (Show, Eq)

integer = Constructor "Integer"
arrow = Constructor "->"
isArrow (Constructor "->") = True
isArrow _ = False
function a = Apply (Apply arrow a)
tuple = Record . zip (map Label.Index [0..])  


recordMatch :: Label -> [(Label, Type)] -> Maybe Type
recordMatch = lookup

functionMatch (Apply (Apply c a) r) b | isArrow c && a == b = Just r
functionMatch _ _ = Nothing

