module Obelus.Infer.Unify (unify, apply, Substitution) where

import Data.Map (Map)
import qualified Data.Map as Map

import Obelus.Infer.Type (Type, Variable)
import qualified Obelus.Infer.Type as Type

type Substitution = Map Variable Type

unify :: Type -> Type -> Maybe Substitution
unify (Type.Variable v) t = bind v t
unify t (Type.Variable v) = bind v t
unify (Type.Apply l1 r1) (Type.Apply l2 r2) = do
    s1 <- unify l1 l2
    s2 <- unify (apply s1 r1) (apply s1 r2)
    Just (s2 `Map.union` s1)
unify (Type.Constructor c1) (Type.Constructor c2) | c1 == c2 = Just Map.empty
unify _ _ = Nothing

bind :: Variable -> Type -> Maybe Substitution
bind v (Type.Variable v2) | v == v2 = Just Map.empty
bind v t = do
    occursCheck v t
    Just (Map.singleton v t)

occursCheck :: Variable -> Type -> Maybe ()
occursCheck v (Type.Variable v2) | v == v2 = Nothing
occursCheck v (Type.Apply l r) = do
    occursCheck v l
    occursCheck v r
occursCheck _ _ = Just ()

apply :: Substitution -> Type -> Type
apply s t@(Type.Variable v) = Map.findWithDefault t v s
apply s (Type.Apply l r) = Type.Apply (apply s l) (apply s r)
apply _ t = t