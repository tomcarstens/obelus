module Obelus.Infer.Infer where

import Data.IORef
import Data.Map (Map)
import qualified Data.Map as Map

import Obelus.Infer.Type (Type, Scheme)
import qualified Obelus.Infer.Type as Type
import Obelus.Infer.Syntax (Syntax)
import qualified Obelus.Infer.Syntax as Syntax
import Obelus.Infer.Solve
import Obelus.Infer.Unify

type Environment = Map Syntax.Variable Type

infer :: Syntax -> IO Scheme
infer s = do
    counter <- newIORef 0
    (t, c) <- generateConstraints counter Map.empty s
    case solve c of
        Just s -> return (Type.generalize (apply s t))
        Nothing -> error "Unification failed"

generateConstraints :: IORef Int -> Environment -> Syntax -> IO (Type, [(Type, Type)])
generateConstraints counter env s =
    case s of
        Syntax.Literal (Syntax.Integer _) -> return (Type.integer, [])
        Syntax.Literal (Syntax.Boolean _) -> return (Type.boolean, [])
        Syntax.Variable v ->
            case Map.lookup v env of
                Just t -> return (t, [])
                Nothing -> error "Undefined variable"
        Syntax.Lambda v b -> do
            vt <- nextVar counter
            (bt, c) <- generateConstraints counter (Map.insert v vt env) b
            return (Type.function vt bt, c)
        Syntax.Apply l r -> do
            (lt, lc) <- generateConstraints counter env l
            (rt, rc) <- generateConstraints counter env r
            tv <- nextVar counter
            return (tv, (lt, Type.function rt tv) : lc ++ rc)

nextVar :: IORef Int -> IO Type
nextVar counter = do
    i <- readIORef counter
    writeIORef counter (i + 1)
    return (Type.Variable ('t' : show i))