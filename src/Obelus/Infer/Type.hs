module Obelus.Infer.Type where

import Data.List(nub)

type Variable = String
type Name = String

data Constructor
    = Integer
    | Boolean
    | Arrow
    | UserDefined Name
    deriving (Show, Eq)

data Type
    = Variable Variable
    | Constructor Constructor
    | Apply Type Type
    deriving (Show, Eq)

data Scheme
    = All [Variable] Type
    deriving (Show, Eq)

integer = Constructor Integer
boolean = Constructor Boolean
arrow = Constructor Arrow
function a = Apply (Apply arrow a)

generalize :: Type -> Scheme
generalize t =
    let
        ftv = nub (freeTypeVariables t)
        nftv = normalize ftv
    in
        All nftv (apply (zip ftv nftv) t)
  where
    normalize tvs = map (('t':) . show) (take (length tvs) [0..])
    apply subst t@(Variable v) =
        case lookup v subst of
            Just v' -> Variable v'
            Nothing -> t
    apply subst (Apply l r) = Apply (apply subst l) (apply subst r)
    apply subst t = t

freeTypeVariables (Variable v) = [v]
freeTypeVariables (Apply l r) = freeTypeVariables l ++ freeTypeVariables r
freeTypeVariables _ = []