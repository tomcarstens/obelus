module Obelus.Infer.Syntax where

type Variable = String

data Literal
    = Integer Integer
    | Boolean Bool
    deriving (Show, Eq)

data Syntax
    = Literal Literal
    | Variable Variable
    | Lambda Variable Syntax
    | Apply Syntax Syntax
    deriving (Show, Eq)