module Obelus.Infer.Solve where

import Control.Arrow ((***))
import Data.Map (Map)
import qualified Data.Map as Map

import Obelus.Infer.Type (Type)
import Obelus.Infer.Unify

solve :: [(Type, Type)] -> Maybe Substitution
solve [] = Just Map.empty
solve ((t1, t2):r) = do
    s <- unify t1 t2
    s2 <- solve (map (apply s *** apply s) r)
    Just (s `Map.union` s2)