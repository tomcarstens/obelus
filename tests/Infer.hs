module Infer (inferTests) where

import qualified Data.Map as Map

import Test.Tasty
import Test.Tasty.HUnit
import qualified Test.Tasty.QuickCheck as QC

import Obelus.Infer.Type (Type)
import Obelus.Infer.Syntax (Syntax)
import qualified Obelus.Infer.Type as Type
import qualified Obelus.Infer.Syntax as Syntax
import Obelus.Infer.Infer

inferTests = testGroup "Infer" [properties, unitTests]

--
-- Properties
--
properties = testGroup "Properties" [typeCheckProperties]

typeCheckProperties = testGroup "Type Checking"
  [ ]

--
-- Unit Tests
--
unitTests = testGroup "Unit Tests" [typeCheckTests]

typeCheckTests = testGroup "Type Checking"
  [ testCase ":t { x -> x } :=> all t0. t0 -> t0" $ do
    s <- infer identity
    s @?= Type.All ["t0"] (Type.function (Type.Variable "t0") (Type.Variable "t0"))
  , testCase ":t { x -> x } 1 :=> Integer" $ do
    s <- infer (Syntax.Apply identity (Syntax.Literal (Syntax.Integer 1)))
    s @?= Type.All [] Type.integer
  ]

identity = Syntax.Lambda "x" (Syntax.Variable "x")