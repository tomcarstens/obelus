module Main where

import Test.Tasty

import Core
import Infer

main = defaultMain tests

tests = testGroup "Tests" [coreTests, inferTests]
