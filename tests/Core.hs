module Core (coreTests) where

import qualified Data.Map as Map

import Test.Tasty
import Test.Tasty.HUnit
import qualified Test.Tasty.QuickCheck as QC

import Obelus.Core.Label (Label)
import qualified Obelus.Core.Label as Label
import Obelus.Core.Syntax (Syntax)
import qualified Obelus.Core.Syntax as Syntax
import Obelus.Core.Type (Type)
import qualified Obelus.Core.Type as Type
import Obelus.Core.Check
import Obelus.Core.Evaluate

coreTests = testGroup "Core" [properties, unitTests]

--
-- Properties
--
properties = testGroup "Properties" [typeCheckProperties, evaluationProperties]

typeCheckProperties = testGroup "Type Checking"
  [ QC.testProperty "for all i : Integer; :t i :=> Integer" $
    \ x -> typeCheck mempty (Syntax.Integer x) == Just Type.integer
  , QC.testProperty "{[x : Integer]}; :t case 1 of { _ -> x } :=> Integer" $ -- wildcard does not alter context
    \ x -> typeCheck (Map.singleton x Type.integer) (Syntax.Case (Syntax.Integer 1) [(Syntax.WildcardPattern, Syntax.Variable x)]) == Just Type.integer
  ]
  
evaluationProperties = testGroup "Evaluation"
  [ QC.testProperty "for all i : Integer; i :=> i" $
    \ x -> evaluate mempty (Syntax.Integer x) == Just (Integer x)
  , QC.testProperty "for all i : Integer, x : string; {[x=i]}; case 1 of _ -> x :=> i" $ -- wildcard does not alter context
    \ x i -> evaluate (Map.singleton x (Integer i)) (Syntax.Case (Syntax.Integer 1) [(Syntax.WildcardPattern, Syntax.Variable x)]) == Just (Integer i)
  ]

--
-- Unit Tests
--
unitTests = testGroup "Unit Tests" [typeCheckTests, evaluationTests]

typeCheckTests = testGroup "Type Checking"
  [ testCase ":t let x = 1 in x :=> Integer" $
    typeCheck mempty simpleLet @?= Just Type.integer
  , testCase ":t { x : Integer -> x } :=> Integer -> Integer" $
    typeCheck mempty identity @?= 
    Just (Type.function Type.integer Type.integer)
  , testCase ":t { x : Integer -> x } 1 :=> Integer" $
    typeCheck mempty (Syntax.Apply identity (Syntax.Integer 1)) @?= 
    Just Type.integer
  , testCase ":t {foo = 1, bar = 2} :=> {foo : Integer, bar : Integer}" $
    typeCheck mempty basicRecord @?= Just basicRecordType
  , testCase ":t { x : {foo:Integer, bar:Integer} -> x.foo } :=> {foo:Integer, bar:Integer} -> Integer" $
    typeCheck mempty recordLambdaProject @?= Just (Type.function basicRecordType Type.integer)
  , testCase ":t { {foo=f, bar=b} : {foo:Integer, bar:Integer} -> x.foo } :=> {foo:Integer, bar:Integer} -> Integer" $
    typeCheck mempty recordLambdaProject @?= Just (Type.function basicRecordType Type.integer)
  , testCase ":t { {foo=f} : {foo:Integer, bar:Integer} -> x.foo } :=> {foo:Integer, bar:Integer} -> Integer" $
    typeCheck mempty recordLambdaProject @?= Just (Type.function basicRecordType Type.integer)
    , testCase ":t { x : {foo:Integer, bar:Integer} -> x.foo } {foo=1, bar=2} :=> Integer" $
    typeCheck mempty (Syntax.Apply recordLambdaProject basicRecord) @?= Just Type.integer
  , testCase ":t { {foo=f, bar=b} : {foo:Integer, bar:Integer} -> x.foo } {foo=1, bar=2} :=> Integer" $
    typeCheck mempty (Syntax.Apply recordLambdaProject basicRecord) @?= Just Type.integer
  , testCase ":t { {foo=f} : {foo:Integer, bar:Integer} -> x.foo } {foo=1, bar=2} :=> Integer" $
    typeCheck mempty (Syntax.Apply recordLambdaProject basicRecord) @?= Just Type.integer
  , testCase ":t (Foo 1 : <Foo:Integer,Bar:Integer> :=> <Foo : Integer, Bar : Integer>" $
    typeCheck mempty basicSum @?= Just basicSumType
  , testCase ":t case Foo 1 : <Foo:Integer, Bar:Integer> of { Foo x -> x; Bar x -> x } :=> Integer" $
    typeCheck mempty basicCase @?= Just Type.integer
  ]
  
evaluationTests = testGroup "Evaluation"
  [ testCase "let x = 1 in x :=> 1" $
    evaluate mempty simpleLet @?= Just (Integer 1)
  , testCase "{ x : Integer -> x } :=> <closure>" $
    evaluate mempty identity @?= 
    Just (Closure (Syntax.VariablePattern "x") (Syntax.Variable "x") mempty)
  , testCase "{ x : Integer -> x } 1 :=> 1" $
    evaluate mempty (Syntax.Apply identity (Syntax.Integer 1)) @?= 
    Just (Integer 1)
  , testCase "{foo = 1, bar = 2} :=> {foo = 1, bar = 1}" $
    evaluate mempty basicRecord @?= Just basicRecordValue
  , testCase "{ x : {foo:Integer, bar:Integer} -> x.foo } {foo=1, bar=2} :=> 1" $
    evaluate mempty (Syntax.Apply recordLambdaProject basicRecord) @?= Just (Integer 1)
  , testCase "{ {foo=f, bar=b} : {foo:Integer, bar:Integer} -> x.foo } {foo=1, bar=2} :=> 1" $
    evaluate mempty (Syntax.Apply recordLambdaProject basicRecord) @?= Just (Integer 1)
  , testCase "{ {foo=f} : {foo:Integer, bar:Integer} -> x.foo } {foo=1, bar=2} :=> 1" $
    evaluate mempty (Syntax.Apply recordLambdaProject basicRecord) @?= Just (Integer 1)
  , testCase "(Foo 1 : <Foo:Integer,Bar:Integer> :=> <Foo 1>" $
    evaluate mempty basicSum @?= Just (Tag "Foo" [Integer 1])
  , testCase "case Foo 1 : <Foo:Integer, Bar:Integer> of { Foo x -> x; Bar x -> x } :=> 1" $
    evaluate mempty basicCase @?= Just (Integer 1)
  ]
  
identity = Syntax.Lambda (svp "x") Type.integer (Syntax.Variable "x")
simpleLet = Syntax.Let (svp "x") (Syntax.Integer 1) (Syntax.Variable "x")
basicRecord = 
  Syntax.Record 
    [ (Label.Label "foo", Syntax.Integer 1)
    , (Label.Label "bar", Syntax.Integer 2)
    ]
basicRecordType =
  Type.Record
    [ (Label.Label "foo", Type.integer)
    , (Label.Label "bar", Type.integer)
    ]
basicRecordValue =
  Record
    [ (Label.Label "foo", Integer 1)
    , (Label.Label "bar", Integer 2)
    ]
    
basicSumType = Type.Sum [("Foo", [Type.integer]), ("Bar", [Type.integer])]
basicSum = Syntax.Tag "Foo" [Syntax.Integer 1] basicSumType

basicCase = Syntax.Case basicSum [(ssp "Foo" [svp "x"], Syntax.Variable "x"), (ssp "Bar" [svp "x"], Syntax.Variable "x")]

recordLambdaProject = Syntax.Lambda (svp "x") basicRecordType (Syntax.Project (Syntax.Variable "x") (Label.Label "foo"))
recordLambdaMatch = Syntax.Lambda (srp [(Label.Label "foo", svp "f")]) basicRecordType (Syntax.Variable "f")
recordLambdaMatchFull = Syntax.Lambda (srp [(Label.Label "foo", svp "f"), (Label.Label "bar", svp "b")]) basicRecordType (Syntax.Variable "f")

svp = Syntax.VariablePattern
srp = Syntax.RecordPattern
ssp = Syntax.SumPattern